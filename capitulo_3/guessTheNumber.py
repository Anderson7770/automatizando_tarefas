# Este é o jogo de adivinhar
import random
secretNumber = random.randint(1, 21)
print('Eu estou pensando em um numero entre 1 e 20.')

# Peça ao jogador para adivinhar o numero 6 vezes
for guessesTaken in range(1, 7):
    print('Take a Guess.')
    guess = int(input("Digite um numero :"))

    if guess < secretNumber:
        print('Voce digitou um numero muito baixo!')
    elif guess > secretNumber:
        print('Voce digitou um numero muito auto.')
    else:
        break # este condição corresponde ao palpite correto
if guess == secretNumber:
    print('Good Job! Voce acertou o meu numero in' + str(guessesTaken)+' guesses')
else:
    print('Nope. The number I was thinking of was' +str(secretNumber))
