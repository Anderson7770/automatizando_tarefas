def variavel_local_global():
    """
    Para simplificar sua vida, evite usar variáveis locais que
    tenham o mesmo nome que uma variável global ou outra variável
    local. Porém, tecnicamente, é perfeitamente permitido fazer
    isso em Python. Para ver o que acontece, digite o código a
    seguir no editor de arquivo e salve-o como sameName.py:
        Como essas três variáveis diferentes têm o mesmo nome,
    pode ser confuso perceber qual delas está sendo usada em
    determinado instante. É por isso que devemos evitar o uso
    do mesmo nome de variável em escopos diferentes.
    """

    pass

def spam():
    eggs = 'Spam Local'
    print(eggs)

def bacom():
    eggs = 'Bacon Local'
    spam()
    print(eggs)

eggs = 'global'
bacom()
print(eggs)
