## Encerrando previamente um programa com sys.exit()
# Podemos encerrar um programa chamando a função sys.exit()
# Esta função se encontrar no modulo sys, devemos importa-la

import sys

while True:
    print("Digite sair para sair")
    response = input()
    if response == 'exit':
        sys.exit()
    print("Você Digitou " + response + ".")

while True:
    print("Qie e voce?")
    name = input()
    if name == 'Anderson':
        continue
    print("hello ", name , "Digite sua senha.")
    senha = input()
    if senha == "123":
        break
print("Finish")

# Resumo
# Ao usar expressões que são avaliadas como True ou False
# (também chamadas de condições), podemos criar programas que
# tomem decisões em relação ao código a ser executado e ignorado
# Também podemos executar códigos repetidamente em um loop
# enquanto determinada condição for avaliada como True. As
# instruções break e continue serão úteis caso você precise sair
# de um loop ou voltar para o início.