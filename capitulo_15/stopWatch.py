# O programa de cronômetro deverá usar o horário atual, portanto será
# necessário importar o módulo time. Seu programa também deverá exibir
# algumas instruções básicas ao usuário antes de chamar input(), portanto o
# timer poderá ser iniciado após o usuário teclar ENTER . Em seguida, o código
# começará a monitorar os tempos de duração das rodadas.
# Digite o código a seguir no editor de arquivo, escrevendo um comentário
# TODO como placeholder para o restante do código

#! python3
# stopWatch.py - Um programa simples de cronometro

# PASSO 1º CRIANDO CRONOMETRO
import time

# Exibe as instruções do programa
print("Press ENTER para iniciar. Afterwards, press ENTER para 'click' to stopwatch. Press Crtl-C to quit.")
quit() # aperte enter para começar
print("Started.")
startTime = time.time() # obtem o primeiro horario de inicio da rodado
lastTime = startTime
lapNum = 1

# PASSO 2: Monitorar e exibir os tempos de duração das rodadas
try:
    input()
    # esta variaavel vai diminuir tempo inicial por ultimo tempo e arredondar com round()
    lapTime = round(time.time() - lastTime, 2)
    totalTime = round(time.time()- startTime, 2)
    print('Lap #%s: %s(%s)' % (lapNum, totalTime, lapTime), end='')
    lapTime += 1
    # reinicial a ultima rodada
    lastTime = time.time()

except KeyboardInterrupt:
    # Trata a exceção de Ctrl-C para evitar que minha msg de erro seja exibida
    print('\nDone.')