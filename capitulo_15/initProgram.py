import time

print("Press ENTER para iniciar. Depois press ENTER\n"
      "para 'click' to stopwatch. Press Crtl-C to quit")
quit()
print("Started.")
startTime = time.time() # primeiro horario
lastTime = startTime
lapNum = 1

# monitorando o tempo
try:
    input()
    lapTime = round(time.time() - lastTime, 2)
    totalTime = round(time.time() - startTime, 2)
    print('Lap #%s: %s(%s)' % (lapNum, totalTime, lapTime), end='')
    lapTime += 1
    # inicia a ultime rodadoa
    lastTime = time.time()

except KeyboardInterrupt:
    print('\nDone.')