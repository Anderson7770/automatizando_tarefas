# o loop while executa a condição enquanto ela for verdadeira,
# mas ao é recomendado para executar uma condição em um determinado numero de
# vezes, por isso o comando apropriado pra isso é o comando for
# veja o exemplo

print('Meu nome é ')
for i in range(5):
    print('Anderson Five Times (' + str(i) + ')')

# Devemos observar que a função range(5) irá repetir o comando 5 vezes
# print('Anderson Five Times (' + str(i) + ')') cujo o resultado será esse:
# Meu nome é
# Anderson Five Times (0)
# Anderson Five Times (1)
# Anderson Five Times (2)
# Anderson Five Times (3)
# Anderson Five Times (4)
# o interessante observar é que a letra 'i' assumirá os valores de 0 até o numero
# inteiro dentro da função range(5), e depois de executar 5x a função ele irá sair do loop

# OBS = a instrução continue e break tambem podem ser usadas juntas com o comando for
# lembrando tb que os comandos break e continue so podem ser usados dentro de loops
# while e for, se for usados em outras condições de codigo o python ixibira um erro.

# exemplo usso do for

total = 0
for num in range(101): # numero de vezes que eu quero que a funçao seja executada o num e a contagem seguencial 1,2,3,4,5...
    total = total + num # aqui o total recebe a soma de total + num
print(total)

# equivalente com while

print('Meu nome é ')
i = 0
while i < 5:
    print('Joselita Five time (' + str(i) + ')')
    i = i + 1


## Argumentos de inicio e fim de incremento de range()
# dentro do range() posso chamar varios argumentos
# separando eles por virgula, de modo que ele possa seguir
# varias seguencias de inteiros, começando com 0 e outro
# numero diferrente de zero
# EXEMPLO
print()
for i in range(0, 16):
    print(i)

# ou com esta outra seguencia
print()
for i in range(12, 20):
    print(i)

# ou tambem pode ser assim
print()
for i in range(10, 15):
    print(i)

## A funcao range() pode ser chamada com 3 argumentos. Os dois primeiros
# serão os valores de inicio e fim, e o terceiro argumento será
# o argumento de incremento
# EXEMPLO
print()
for i in range(1, 5, 2): # obs = no caso aqui o num. 2 seria o incremento
    print('chamada do range com 3 argumentos, range(num1, num2, num3), sendo o num3 o argumento de incremento')
    print(i)

## Fator interessante sobre função range() => nos podemos passar
# para a função numeros negativos(ex. -1, -10) para que ele possa
# fazer o incremento na ordem decrescente, segue o exemplo abaixo:
# EXEMPLO
print()
for i in range(5, -2, -1): # obs : para poder funcionar o incremento negativo os 2 ultimos numeros devem ser negativos
    print(" Teste do incremento negativo!")
    print(i)