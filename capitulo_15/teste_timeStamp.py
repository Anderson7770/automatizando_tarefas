import time

def calcProd():
    # calcula o produto dos 100.000 primerios numeros
    produto = 1
    for i in range(1, 100000):
        produto = produto * i
    return produto

tempo_inicial = time.time() ## aqui é o inicio da contagem de execução de calcProd()
prod = calcProd() ### aqui a função sera executada
tempo_final = time.time()## Aqui e o final da execução da função calcProd()
print(" The o resultado e %s digits long." % (len(str(prod))))
print("Took %s seconds to calculate." % (tempo_final - tempo_inicial))

#########################################################################################
# Para que o programa permanessa parado e necessario chamar a função time.sleep('O numero de segundos
# que o programa ficara parado')
# EXEMPLO:
import time

for i in range(10):
    print("Começou")
    time.sleep(10)
    print("2 Começou a segunda vez!")
    time.sleep(20)
    print("Começou 3º vez!")
    time.sleep(30)

# Arredondando o tempo com round()
now = time.time()
print(now)
now2 = round(now)
print(now2)

