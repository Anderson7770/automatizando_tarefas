import time
print("Press ENTER to Init. After press ENTER \n"
      "para 'click' to stop. Press Ctrl-C to quit.")
quit()
print("Started.")
startTime = time.time()
lastTime = startTime
lapNum = 1

try:
    input()
    lapTime = round(time.time() - lastTime, 2)
    totalTime = round(time.time() - startTime, 2)
    print('Lap # %s: %s(%s)' % (lapNum, totalTime, lapTime))
    lapTime += 1
    lastTime = time.time()
except KeyboardInterrupt:
    print('\n Done.')