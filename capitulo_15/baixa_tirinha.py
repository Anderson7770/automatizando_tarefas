#!python3
# multidownloadxkcd.py - faz download de tirinhas XKCD
# usando threads.
import requests, os, bs4, threading

#Armazena as tirinhas em ./xkcd
os.makedirs('xkcd', exist_ok=True)

def downloadXkcd(startComic, endComic):
    for urlNumber in range(startComic, endComic):
        # Faz download da pagina
        print('Downloading page http://xkcd.com/%s' % (urlNumber))
        res = requests.get('http://xkcd.com/%s' % (urlNumber))
        res.raise_for_status()
        soup = bs4.BeautifulSoup(res.text)

        # encontra o url da imagem da tirinha
        comicElem = soup.select('#comic img')
        if comicElem == []:
            print('Cound not find comic imagem.')
        else:
            comicUrl = comicElem[0].get('src')
            # Faz o download da imagem
            print('Downloading image %s...'% (comicUrl))
        res = requests.get(comicUrl)
        res.raise_for_status()
        # Salva a imagem em ./xlcd
        imageFile = open(os.path.join('xkcd', os.path.asename(comicUrl)), 'wb')
        for chunk in res.iter_content(10000):
            imageFile.write(chunk)
            imageFile.close()

downloadThreads = []
for i in range(0, 1400, 100):
    downloadThread = threading.Thread(target=downloadThread, args=(i, i + 99))
    downloadThreads.append(downloadThread)
    downloadThread.start()
for downloadThread in downloadThreads:
    downloadThreads.join()
    print('Done.')

