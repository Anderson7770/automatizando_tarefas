import time
print('Press Enter to Init.\n'
      'Press ENTER to Click.\n'
      'Press Ctrl-C to quit.')
quit()
print("Started.")
startTime = time.time()
lastTime = startTime
lapNum = 1

try:
    input()
    lapTime = round(time.time() - lastTime, 2)
    totalTime = round(time.time() - startTime, 2)
    print('Lap # %s: %s(%s)' % (lapNum, totalTime, lapTime))

except KeyboardInterrupt:
    print('OK Terminado')