# IMPORTANDO MODULOS EM PYTHON
# O Python também vem com um conjunto de
# módulos chamado biblioteca-padrão (standard library).
# Cada módulo corresponde a um programa Python que contém
# um grupo relacionado de funções que pode ser incluído em
# seus programas.
# Antes de poder usar as funções de um módulo, você deve importá-lo
# usando uma instrução import. No código, uma instrução import
# é constituída das seguintes partes:
# • a palavra-chave import;
# • o nome do módulo;
# • opcionalmente, mais nomes de módulos, desde que estejam
# separados por vírgula.

# nome do arquivo printRandom.py

import random
for i in range(5):
    print(random.randint(1, 10))

# obs = a funcao random.randint() ira escolher numeros aleatorios
# que esteja entre os numeros 1 e 10
# obs = a funcao randint() esta dentro do modulo random, por isso
# o nome random vem a frente de randint() exemplo: random.randint()

# obs = quando importamos os modulos python com from nome-do-modulo e
# import nome-da-funcao, isso nos possibilita usar as funçoes sem ter que colocar
# o nome do modulo na frente das chamadas das funçoes.