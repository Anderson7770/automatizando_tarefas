# METODOS IMPORTANTES DO SUBPROCESS

# poll()
# Podemos pensar no método poll() como se você estivesse
# perguntando a uma amiga se ela acabou de executar o código
# que você lhe deu. O método #poll() retornará None se o
# processo ainda estiver executando quando poll() for chamado.
# Se o programa tiver terminado, um inteiro referente ao
# código de saída (exit code) do processo será retornado.

# wait()
# O método wait() é como esperar sua amiga acabar de usar seu código
# antes que você possa trabalhar no seu. O método wait()
# ficará bloqueado até que o processo iniciado tenha terminado.
# Isso é útil se você quiser que seu programa faça uma pausa
# até que o usuário acabe de usar o outro programa. O valor de
# retorno de wait() é um inteiro com o código de saída do
# processo.

import subprocess
prog = subprocess.Popen('/home/anderson/masterpdfeditor4')