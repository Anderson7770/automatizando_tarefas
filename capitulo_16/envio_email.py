# Open a connection in IDLE mode and wait for notifications from the
# server.

from imapclient import IMAPClient
HOST = "imap-mail.outlook.com"
USERNAME = "anderson77martins@outlook.com"
PASSWORD = "b166er123@"

server = IMAPClient(HOST)
server.login(USERNAME, PASSWORD)
server.select_folder("INBOX")

# Start IDLE mode
server.idle()
print("Connection is now in IDLE mode, send yourself an email")

while True:
    try:
        # Wait for up to 30 seconds for an IDLE response
        responses = server.idle_check(timeout=30)
        print("Server sent:", responses if responses else "nothing")
    except KeyboardInterrupt:
        break

server.idle_done()
print("\nIDLE mode done")
server.logout()