# O que são listas
Uma lista é um valor que contém diversos valores em uma sequência
ordenada. O termo valor de lista refere-se à lista propriamente dita (que é um
valor que pode ser armazenado em uma variável ou passado para uma função,
como qualquer outro valor), e não aos valores contidos no valor da lista.

# Tipos de listas
>>>[1, 2, 3]
>>>['cat', 'bat', 'rat', 'elephant']
>>>['hello', 3.4323, True, None, 42]
>>>spam = ['cat', 'bat', 'dog', 'elephant']

A variavel spam armazena uma lista, que por sua vez contem outros valores dentro dela

# Obtendo valores individuais de uma lista
spam = ['cat', 'bat', 'dog', 'elephant']
spam[0]
spam[1]
spam[2]
spam[3]

estes valores sao acessados separadamente em sua posições.
Exemplo 1 spam[0] da acesso a string 'cat', por que é o 1º elemento da lista spam
Exemplo 2 spam[1] da acesso a string 'bat', por que é o 2º elemento da lista
Exemplo 3 spam[2] da acesso a string 'dog', por que é o 3º elemento da lista
