def spam(divideBy):
    """
    Os erros podem ser tratados com instruções try e except,
    O codigo que pode conter um erro em potencial é inserido em uma clausula
    try. A execução do programa segue para o inicio da clausula
    except seguinte caso um erro aconteça. Podemos colocar o
    codigo anterior de divisao por zero em uma clausula
    try e fazer com que uma clausula exxcept contenha um codigo para
    lidar com o que acontece quando esse erro ocorrer.
    """
    try:
        return 42 / divideBy
    except ZeroDivisionError:
        print('Error: Invalid Argument')

print(spam(2))
print(spam(0))
print(spam(30))
print(spam(32))