## CONTROLE DE FLUXO
    O controle de fluxo pode decidir quais as funções serão executadas
ou repetidas, de acordo com a necessidade ou problema apresentado
pelo programa.

    EM um fluxograma é mais uma maneira de ir do inicio ao final de um programa
O mesmo vale para as linha de codigo de um programa de computador.
    Para se aprender bem como umas as instruçoes de controle de fluxoé necessario
primeiro saber utilizar o fluxograma com instruções SIM e NAO.

## Elementos do controle de fluxo
As indtruções do controle de fluxo começam com uma condição e uma cláusula.fluxo
ou Condições e blocos

condições == if elif else
bloco de codigo == linhas de codigo python GRUPADAS

EXEMPLO :
if name == 'Mary':
    print('Hello Mary!')
if password == 'swordfish':
    print('Access granted ')
else:
    print('wrong password.')

## Instrução de loop while
para executar um bloco de codigo repetidamente usamos o while.
Se for usado com o booleano True tipo: while True:
Este bloco será executado ate terminar o loop ou ate encontrar uma condição
false.

EXEMPLO com instrução if
spam = 0
if spam < 5:
    print('Hello, World!)
    spam = spam + 1

EXEMPLO com instrução while
spam = 0
while spam < 5:
    print('Hello World')
    spam = spam + 1

OBS : para a instrução while a saida e hello world repetida 5 vezes

## INSTRUÇÂO BREAK
 A instruções break deve ser usada junto com o loop while para brecar o loop
 infinito quando while é usado da seguinte forma:
 exemplo : while True:
                print('Hello world')
                break


## INSTRUÇÃO CONTINUE
As instruçoes continue são usadas nos loops, assim como a instrução break. Quando
a condição do loop for verdadeira o programa retorna ao inicio instruçoes.
exemplo:
while True:
    print('Who are you?')
    name = input()
    if name == 'Joe':
        continue
    print('Hello ', name, 'Digite sua password!')
    pass = input()
    if pass == 'swordfish':
        break
print("Acesso permitido!")