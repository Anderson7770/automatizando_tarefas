# 9. Escreva um código que exiba Hello se 1 estiver armazenado em
# spam, Howdy se 2 estiver armazenado em spam e Greetings! se outro
# valor estiver armazenado em spam.

spam = input("Digite um numero :")
if spam == '1':
    print("Voce digitou o numero " + str(spam))
    print('hello')

elif spam == '2':
    print("Voce digitou o numero " + str(spam))
    print('Howdy')

else:
    print("Voce digitou o numero " + str(spam))
    print('Greetings')

# EXERCICIO 13
for i in range(1, 11):
    print(i)

# EQUIVALENTE COM WHILE
total = 0
while total < 11:
    total = total + 1
    print('Equivalente ao while ,', total)


# 13. Crie um pequeno programa que mostre os números de 1 a 10 usando um
# loop for. Em seguida, crie um programa equivalente que mostre os números
# de 1 a 10 usando um loop while.
#
# 14. Se você tivesse uma função chamada bacon() em um módulo chamado
# spam, como você a chamaria após ter importado spam?
#
# Pontos extras: Dê uma olhada nas funções round() e abs() na Internet e
# descubra o que
# elas fazem. Faça experimentos com elas no shell interativo.

