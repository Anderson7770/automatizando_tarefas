import random

#def hello(name):
#    print('Hello' + name)
#    return 'OK'

def getAnswer(answerNumber):
    """
    Quando chamamos a função len() e lhe passamos um argumento como
    'Hello', a chamada à função será avaliada como o valor inteiro 5,
    que é o tamanho da string passada. Em geral, o valor com o qual
    uma chamada de função é avaliada é chamado de valor de retorno da função.
    """
    if answerNumber == 1:
        return 'It is certain'
    elif answerNumber == 2:
        return 'It is decidedly so'
    elif answerNumber == 3:
        return 'Yes'
    elif answerNumber == 4:
        return 'Reply hazy try again'
    elif answerNumber == 5:
        return 'Ask Again'
    elif answerNumber == 6:
        return 'Concentrate and ask'
    elif answerNumber == 7:
        return ' My replay is no'
    elif answerNumber == 8:
        return 'Outlook not so good'

    return "Very Doubtful"

r = random.randint(1, 9)
fortune = getAnswer(r)
print(fortune)